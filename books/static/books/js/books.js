$(document).ready(function() {
    M.updateTextFields();
  });

  

$(document).ready(function () {
    $('.submit').submit(function(event){
        let count = 10;
        let searchItem = $('.searchbox').val();
        let urlItem = "https://www.googleapis.com/books/v1/volumes?q=" + searchItem + "&maxResults=" + count;

        $.ajax({
            async: true,
            type: 'GET',
            url: urlItem,
            dataType: 'json',
            success: function(searchRes){
                let bookShelf = searchRes.items;
                $('tbody').empty();

                for(i = 0; i< count; i++){
                    let book = bookShelf[i].volumeInfo;
                    var num = $('<td>').text(i+1);
                    var name = $('<td>').text(book.title);

                    if('authors' in book == false)var authors = $('<td>').text("-");
                    else var authors = $('<td>').text(book.authors);
                    
                    if ('categories' in book == false) var categories = $('<td>').text("-");
                    else var categories = $('<td>').text(book.categories);

                    if ('publisher' in book == false) var publisher = $('<td>').text('-');
                    else var publisher = $('<td>').text(book.publisher);

                    if ('imageLinks' in book == false)
                        var img = $('<td>').text("-");
                    else {
                        if ('smallThumbnail' in book.imageLinks == false    )
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.thumbnail
                            }));
                        else
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.smallThumbnail
                            }));
                    }

                    var tr = $('<tr>').append(num, name, authors,
                        categories, publisher,  img);
                    
                    $('tbody').append(tr);



                }

            },
            error: function() {
                alert("Sorry, there has been an error");
            },
            type: 'GET',
        })
        event.preventDefault();
    });
});

