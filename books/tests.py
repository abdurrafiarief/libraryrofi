from django.test import TestCase, Client, LiveServerTestCase
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class BookTest(TestCase):
    def test_book_url(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "base.html")

class LibraryFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path= './chromedriver')

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

       
    def test_input_todo(self):        
        # Opening the link we want to test
        self.selenium.get(self.live_server_url + '/')
        
        # find the form and button element
        self.selenium.implicitly_wait(10)
        
        searchbox = self.selenium.find_element_by_css_selector(".searchbox")
        self.selenium.implicitly_wait(10)
        searchbox.send_keys("Algorithm")
        
        button = self.selenium.find_element_by_css_selector('.searchbutton')
        button.click()
        button.click()
        
        time.sleep(5)
        self.selenium.implicitly_wait(30)
        self.assertIn("The Master Algorithm", self.selenium.page_source)
        