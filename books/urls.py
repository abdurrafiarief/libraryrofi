from django.urls import path
from .views import landing

app_name = "books"

urlpatterns = [
    path('', landing)
]